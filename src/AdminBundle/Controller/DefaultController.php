<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $ArticleRepo = $em->getRepository('SiteBundle:Article');
        $allarticle = $ArticleRepo->getNbArticle(); 
        return $this->render('AdminBundle:Default:index.html.twig', array('nbarticle' => $allarticle));
    }
}
