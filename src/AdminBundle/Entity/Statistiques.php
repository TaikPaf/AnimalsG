<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statistiques
 *
 * @ORM\Table(name="statistiques")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\StatistiquesRepository")
 */
class Statistiques
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nbArticles", type="integer", nullable=true)
     */
    private $nbArticles;

    /**
     * @var int
     *
     * @ORM\Column(name="nbMembres", type="integer", nullable=true)
     */
    private $nbMembres;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbArticles
     *
     * @param integer $nbArticles
     * @return Statistiques
     */
    public function setNbArticles($nbArticles)
    {
        $this->nbArticles = $nbArticles;

        return $this;
    }

    /**
     * Get nbArticles
     *
     * @return integer 
     */
    public function getNbArticles()
    {
        return $this->nbArticles;
    }

    /**
     * Set nbMembres
     *
     * @param integer $nbMembres
     * @return Statistiques
     */
    public function setNbMembres($nbMembres)
    {
        $this->nbMembres = $nbMembres;

        return $this;
    }

    /**
     * Get nbMembres
     *
     * @return integer 
     */
    public function getNbMembres()
    {
        return $this->nbMembres;
    }
}
